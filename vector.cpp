#include <iostream>
#include <vector>
#include <algorithm>

int main(int argc, char **argv)
{
  std::vector<double> data;
  int size=0;
  
  std::clog << "Size of the array?  ->" <<std::endl;
  std::cin >> size;
  data.resize(size);
  
  for(int ii=0; ii < size; ++ii) {
    data[ii]=2*ii;
  }
  for (int ii=0 ; ii< size;++ii){
    std::cout <<ii <<"\t"<<data[ii]<<std::endl;
  }
  return 0;
}
